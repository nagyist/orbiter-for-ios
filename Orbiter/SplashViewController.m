//
//  SpashViewController.m
//  Orbiter
//
//  Created by Chris Wilson on 31/10/2012.
//  Copyright (c) 2012 Chris Wilson. All rights reserved.
//

#import "SplashViewController.h"
#import "AppDelegate.h"
#import "Launchpad.h"
#import "LaunchpadAuthenticator.h"

@implementation SplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    appDelegate.launchpad = [[Launchpad alloc] init:[appDelegate consumerKey]];
    
    if( ! [appDelegate checkForExistingOAuthCredentials] ){
        appDelegate.launchpad.delegate = self;
        [appDelegate.launchpad getRequestToken];
    } else {
        appDelegate.launchpad.accessTokenSecret = appDelegate.authenticator.accessTokenSecret;
        appDelegate.launchpad.accessToken = appDelegate.authenticator.accessToken;
        appDelegate.launchpad.consumerKey = appDelegate.authenticator.consumerKey;
        
        [self performSegueWithIdentifier:@"AlreadyLoggedInSegue" sender:self];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self beginSplashAnimation];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self endSplashAnimation];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if( [[segue identifier] isEqualToString:@"NotLoggedInSegue"] ){
        LoginViewController *loginViewController = [segue destinationViewController];
        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        
        loginViewController.delegate = self;
        loginViewController.urlToLoad = [appDelegate.launchpad authorisationUrl];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)beginSplashAnimation
{
    
}

- (void)endSplashAnimation
{
    
}

#pragma mark - LoginViewControllerDelegate

- (void)loginViewControllerDidFinish:(LoginViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:^() {
        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [appDelegate.launchpad exchangeRequestTokenForAccessToken];
    } ];
}

#pragma  - LaunchpadDelegate

- (void)requestforRequestTokenDidComplete:(Launchpad *)newLaunchpad
{
    [self performSegueWithIdentifier:@"NotLoggedInSegue" sender:self];
}

- (void)exchangeOfRequestTokenForAccessTokenDidComplete:(Launchpad *)newLaunchpad
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    appDelegate.authenticator = [NSEntityDescription
                                 insertNewObjectForEntityForName:@"LaunchpadAuthenticator"
                                 inManagedObjectContext:context];
    
    appDelegate.authenticator.accessToken = appDelegate.launchpad.accessToken;
    appDelegate.authenticator.accessTokenSecret = appDelegate.launchpad.accessTokenSecret;
    appDelegate.authenticator.consumerKey = appDelegate.launchpad.consumerKey;
    
    NSError *error;
    [context save:&error];
    
    [self performSegueWithIdentifier:@"AlreadyLoggedInSegue" sender:self];
}

- (void)requestforRequestTokenDidFail:(Launchpad *)launchpad withResponse:(NSURLResponse *)response withError:(NSError *)error
{
    // TODO: Gracefully handler errors from Launchpad
}

- (void)exchangeOfRequestTokenForAccessTokenDidFail:(Launchpad *)launchpad withResponse:(NSURLResponse *)response withError:(NSError *)error
{
    // TODO: Gracefully handler errors from Launchpad
}

@end
