//
//  LaunchpadAuthenticator.m
//  Orbiter
//
//  Created by Chris Wilson on 18/01/2013.
//  Copyright (c) 2013 Chris Wilson. All rights reserved.
//

#import "LaunchpadAuthenticator.h"

@implementation LaunchpadAuthenticator

@dynamic consumerKey;

@dynamic accessToken;
@dynamic accessTokenSecret;

@end
