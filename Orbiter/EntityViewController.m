//
//  BugViewController.m
//  Orbiter
//
//  Created by Chris Wilson on 27/10/2012.
//  Copyright (c) 2012 Chris Wilson. All rights reserved.
//

#import "EntityViewController.h"

@interface EntityViewController ()

@end

@implementation EntityViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
