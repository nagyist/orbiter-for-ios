//
//  LaunchpadAuthenticator.h
//  Orbiter
//
//  Created by Chris Wilson on 18/01/2013.
//  Copyright (c) 2013 Chris Wilson. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "Launchpad.h"

@interface LaunchpadAuthenticator : NSManagedObject

@property (nonatomic, retain) NSString * consumerKey;
@property (nonatomic, retain) NSString * accessToken;
@property (nonatomic, retain) NSString * accessTokenSecret;

@end
