//
//  OrbiterViewController.m
//  Orbiter
//
//  Created by Chris Wilson on 07/11/2012.
//  Copyright (c) 2012 Chris Wilson. All rights reserved.
//

#import "OrbiterViewController.h"

@interface OrbiterViewController ()

@end

@implementation OrbiterViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
