//
//  DashboardViewController.m
//  Orbiter
//
//  Created by Chris Wilson on 01/11/2012.
//  Copyright (c) 2012 Chris Wilson. All rights reserved.
//

#import "DashboardViewController.h"

#import "AppDelegate.h"
#import "PersonFactory.h"
#import "Person.h"

@interface DashboardViewController ()

@end

@implementation DashboardViewController
@synthesize userIdCell = _userIdCell;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    PersonFactory *factory = [[PersonFactory alloc] init:[appDelegate launchpad]];
    Person *authenticatedUser = [factory manufactureAuthenticatedUser];
    
    _userIdCell.textLabel.text = [authenticatedUser displayName];
    _userIdCell.imageView.image = [UIImage imageWithData:[authenticatedUser logoData]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
