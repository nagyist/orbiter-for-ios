//
//  AppDelegate.h
//  Orbiter
//
//  Created by Chris Wilson on 24/10/2012.
//  Copyright (c) 2012 Chris Wilson. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LaunchpadAuthenticator;
@class Launchpad;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) LaunchpadAuthenticator *authenticator;
@property (strong, nonatomic) Launchpad *launchpad;

@property (strong, nonatomic) NSString *consumerKey;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

- (Boolean)checkForExistingOAuthCredentials;

@end
