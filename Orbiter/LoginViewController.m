//
//  LoginViewController.m
//  Orbiter
//
//  Created by Chris Wilson on 04/12/2012.
//  Copyright (c) 2012 Chris Wilson. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
#import "Launchpad.h"

@implementation LoginViewController

@synthesize delegate;

@synthesize loginWebView = _loginWebView;
@synthesize urlToLoad = _urlToLoad;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_loginWebView loadRequest:[NSURLRequest requestWithURL:_urlToLoad]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)done:(id)sender
{
    [self.delegate loginViewControllerDidFinish:self];
}

@end
