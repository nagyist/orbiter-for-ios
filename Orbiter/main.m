//
//  main.m
//  Orbiter
//
//  Created by Chris Wilson on 24/10/2012.
//  Copyright (c) 2012 Chris Wilson. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
