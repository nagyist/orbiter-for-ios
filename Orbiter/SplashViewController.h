//
//  SpashViewController.h
//  Orbiter
//
//  Created by Chris Wilson on 31/10/2012.
//  Copyright (c) 2012 Chris Wilson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"
#import "Launchpad.h"

@interface SplashViewController : UIViewController <LoginViewControllerDelegate, LaunchpadDelegate>

- (void)beginSplashAnimation;
- (void)endSplashAnimation;

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *splashViewController;
@end
