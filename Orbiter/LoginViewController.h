//
//  LoginViewController.h
//  Orbiter
//
//  Created by Chris Wilson on 04/12/2012.
//  Copyright (c) 2012 Chris Wilson. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LoginViewController;

@protocol LoginViewControllerDelegate <NSObject>
- (void)loginViewControllerDidFinish:(LoginViewController *)controller;
@end

@interface LoginViewController : UIViewController

@property (nonatomic, weak) id <LoginViewControllerDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIWebView *loginWebView;
@property (strong, nonatomic) NSURL *urlToLoad;

- (IBAction)done:(id)sender;

@end
